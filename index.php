<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en"><head>
    <title>daa.test.dev4.digital-spectr.ru &mdash; Coming Soon</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="description" content="This is a default index page for a new domain."/>
    <style type="text/css">
        body {font-size:14px; color:#777777; font-family:arial; text-align:center;}
        h1 {font-size:50px; color:#555555; margin: 70px 0 50px 0;}
        p {width:320px; text-align:center; margin-left:auto;margin-right:auto; margin-top: 30px }
        div {width:320px; text-align:left; margin-left:auto;margin-right:auto;}
        table {width:320px; border: 1px solid #333; margin: 0 auto 20px auto;}
        table tr, td {padding: 5px;}
        a:link {color: #34536A;}
        a:visited {color: #34536A;}
        a:active {color: #34536A;}
        a:hover {color: #34536A;}
    </style>
</head>
<body>
<h1>daa.test.dev4.digital-spectr.ru</h1>
<div>
    <form method="get" action="/headers.php">
        <input type="text" name="param">
        <button type="submit">Отправить</button>
    </form>
</div>
<br>
<?php

require 'dbparams.php';

// подключение к базе данных
$dbconn = new mysqli($host, $user, $password, $database);
if ($dbconn->connect_error) {
    printf("Не удалось подключиться: $dbconn->connect_error");
    exit();
}

// параметр страны
$countryId = 1; // 1-Россия, 2-США, 3-Китай

// запрос к базе данных
$query = "SELECT NAME FROM cities WHERE COUNTRY_ID = $countryId";
$result = $dbconn->query($query);
if($result)
{
    // количество полученных строк
    $rows = $result->num_rows;

    // количество полученных записей
    $cols = mysqli_num_fields($result);

    echo "<table><tr><th>Город</th></tr>";
    for ($i = 0; $i < $rows; ++$i)
    {
        // возвращает строку данных в виде массива
        $row = $result->fetch_row();
        echo "<tr>";
        for ($j = 0; $j < $cols; ++$j) echo "<td>$row[$j]</td>";
        echo "</tr>";
    }
    echo "</table>";

    print_r($result);

    // очистка результата
    $result->free();

    // закрытие соединения с базой данных
    $dbconn->close();
}
else
{
    echo 'Ошибка в запросе';
}

?>
</body>
</html>